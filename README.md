## **EXPAT**

_EXtension-based Private Advertising Technology_

## Background

The topic of _privacy-preserving ad targeting_ is at the heart of discourse in online advertising. In November 2020, we
posted our first proposal for performing privacy-preserving ad targeting,
called [SPECTACLE](https://gitlab.com/eyeo/pat/spectacle), short for _“Sensible Privacy Enablement by Clustering
Targeting Attributes in CLiEnt”_. SPECTACLE called for utilizing an installable browser extension or other software to
establish an equilibrium of online value exchange, providing: (1) novel privacy protection for users, beyond other free
software and (2) a privacy-preserving ad targeting system for publishers, advertisers, and marketing platforms.

In this document, we evolve our previous proposal with a more tangible and comprehensive approach that would enable
interested parties to actively participate and examine this concept in a “real” environment of online browsing,
including an API proposal, access management, and additional security and privacy considerations to solve a range of
expected scenarios. We intend to integrate this technology with an ad-filtering software extension, though we are
sharing it as a set of proposals that could be implemented in other extensions or even integrated into a browser, mobile
app, or smart TV OS. Given the planned extension-based initial implementation, we named this proposal “EXPAT”, for “
_Extension-based Private Advertising Technology”_. We also voice support for other relevant industry standards and
proposals, aiming to adopt those to support EXPAT where it makes sense, rather than adding more proposals to a crowded
space.

## Vision – What We Intend to Solve

As cookie-based trackers near their end, and data protection legislation continues to rise, we would like to empower
online users with an honest web, where the rules of engagement are clear and transparent, and users’ rights are
protected, but so is the value exchange between content/service providers and audiences – a healthy ad-supported
ecosystem. We propose to fulfill this vision by introducing a standardized and consistent way for users of any browser
to **understand** and **determine** the data signals that are shared as they engage with the web economy. These
capabilities can be achieved through an extension, such as (but not limited to) an ad and trackers filtering tool.

By providing users with a consistent and complete set of tools to ensure they remain unidentified across sites as
specific individuals, while informing them transparently about the usage of their information, an enhanced value
exchange can be established between content and its audience, as well as content/service providers and their users.
Greater **transparency**, **control**, and assurances of **user privacy** can enable and protect leeway to share
insightful information that benefits the web economy.

An ad filterer is arguably a good candidate for an initial implementation of EXPAT, given its role as an additional
layer to the browser that facilitates the user’s engagement with the world wide web. In other words, an ad filterer is
an extension (not in the software sense) to the user agent, and hence, a user agent by itself. Given this role relative
to users, extensions such as ad filterers could support criteria-based controls and filtering for tracking and
targeting, similar to the approach already in place for ad rendering/filtering.

## Principles and Goals

In considering which specific proposals to include in this document, we defined a number of principles and goals as
evaluation criteria to apply to candidates – and also to existing standards and proposals. The results of this exercise
led to paring down our new proposals and adding support for some relevant industry standards and proposals.

### Principles

1. **User Control:** Users should be in control of their data and privacy choices, with Transparency, Predictability,
   Data minimization/limitation, and the Right to be Forgotten.
2. **Interoperability:** Each solution should enable consistent, interoperable implementation across environments (
   devices, platforms, geographies, publishers) as much as possible.
3. **Minimal “Reinvention”:** To ensure adoption with relatively modest means (by both larger and smaller businesses),
   proposals and solutions should build on existing infrastructure and should avoid replicating existing proposals that
   are sufficient to solve identified use cases.
4. **Zero-trust (among industry entities):** Solutions should assume/deploy technology-based zero-trust models, across
   users, advertisers, and publishers.
5. **Solution Evolution:** Solutions can and should improve over time, as laws/regulations and platform policies,
   privacy-enhancing technologies, and best practices evolve to support stronger privacy protection and data security.
6. **Fair Value Exchange:** Solutions should enable equitable remuneration for making online content broadly and readily
   available.

### Goals

1. **Purpose-built:** The proposal effectively addresses a clear <span style="text-decoration:underline;">advertising
   use case</span>:
    1. <span style="text-decoration:underline;">Privacy Protection</span> (for users): Strengthen cross-site/app privacy
       boundaries and prevent covert tracking
    2. <span style="text-decoration:underline;">Targeting</span>: Show relevant content and ads
    3. <span style="text-decoration:underline;">Measurement</span>: Measure and attribute digital ad impact/ROI
    4. <span style="text-decoration:underline;">Spam/Fraud Reduction</span>: Support fighting digital spam and fraud
    5. <span style="text-decoration:underline;">Compliance</span> (for businesses): Reducing exposure and/or liability
       as a result of violating privacy legislation
2. **Cooperation:** The proposal can foster collaboration across the industry, resulting in integration into multiple
   solutions and/or platforms (not just serving one platform).
3. **Perception:** The proposal can help drive a positive perception of industry participants – demonstrating that we’re
   addressing both user experience and privacy needs.
4. **Principle Alignment:** The proposal helps achieve our stated principles, benefiting users and businesses globally.

## EXPAT Proposals by Use Case

### Privacy Protection / Compliance

#### Proxying Traffic

**Principles/Goals alignment**:

* Purpose-built for Privacy Protection and Compliance, but have to be taken into account in combination with “Location
  Service” below.
* Could support Cooperation (there are some competing efforts).
* Fosters positive Perception.
* Strongly aligns with the first five Principles described above (Fair Value Exchange is addressed better by sections
  referring to ad targeting below).

Advertising systems can make household-level targeting decisions based on the IP address of the connecting client.
Proxying traffic prevents the origin IP from being available to the ad system. Moreover, hiding the client’s IP address
reduces the likelihood of fingerprinting. However, this has implications in terms of operational costs and performance.

Sending only bidding and advertising traffic through a privacy proxy, while allowing non-advertising traffic to flow
directly to the servers, can optimize for privacy, performance, and reduced operational costs.

![Proxy traffic diagram](images/proxy_traffic.png "image_tooltip")

<sub><strong>Diagram 1: All advertising traffic is proxied in order to prevent the user’s IP Address from being utilized
as a semi-persistent targeting surface </strong></sub>

Concerns about collecting data on the server-side might be raised, but the client-side code has access to more
information in such a configuration. Maintaining a list of advertising systems is already done
by[ multiple entities](https://disconnect.me/trackerprotection).

#### Cookie/Storage Partitioning

**Principles/Goals alignment**:

* Purpose-built for Privacy Protection.
* Has some challenges with Cooperation (different approaches by each browser).
* Fosters positive Perception.
* Generally aligns with the first five Principles described above and partially with Fair Value Exchange (the generator
  of the content, the publisher, retains access to first-party data)

Cookie storage partitioning is a crucial measure that effectively limits cross-site tracking on the web as third-party
cookies are used to track and collect information about a user's activity across multiple websites, thereby creating a
comprehensive profile of their interests and behavior. By adopting these measures, we will limit this risk exposure and
protect the user’s privacy.

From a user experience point of view, for the time being, partitioning third-party cookies (and practically making them
useless to third parties) provides an improved outcome in comparison to blocking them per se, as still, many websites
rely on third-party cookies for their intended functionality (avoiding “page breaking” or necessary service providers).
By limiting the access of cookies to first-party context only, but not revoking them completely, we ensure that a
website’s functionality remains without change. \
\
A few popular browsers are already heading in this direction. We encourage the efforts invested by
Firefox ([State Partitioning](https://developer.mozilla.org/en-US/docs/Web/Privacy/Storage_access_policy/Errors/CookiePartitionedForeign))
and
Safari ([Full third-party cookies blocking](https://webkit.org/blog/10218/full-third-party-cookie-blocking-and-more/)).
Also, Chrome recently released [CHIPS](https://github.com/privacycg/CHIPS) to tackle this problem with the only caveat
that websites need to opt-in to partition their cookies. Blocking third-party cookies, or making them practically
redundant, is the most fundamental privacy improvement needed to make a browser “privacy-preserving”. In case a browser
maintains an equivalent component, which is turned on by default, turning this feature via extension becomes obsolete.

#### Privacy Signals

**Principles/Goals alignment**:

* Purpose-built for Privacy Protection and partially for Compliance
* Generally fosters Cooperation
* Fosters positive Perception.
* Generally aligns with the six Principles described above.

A privacy signal can enable an individual to express their expectations of how an online third party can treat this
individual's information. However, in most cases, such initiatives, like “Do Not Track” (DNT), have fallen short as
adhering to them was on a volunteer basis. Without mass adoption of such a signal, its effectiveness is very limited.

“[Global Privacy Control” (GPC](https://globalprivacycontrol.org/)), is the descendent of the DNT signal and is a
notable exception to the statement above. It enables users to express their intent that their personal data will not be
shared with third parties. As such, it aligns well with the privacy goal that is listed above. This signal is developed
as a specification for passing universal opt-out signals, transmitted over HTTP and through the DOM. GPC is already
adopted by browsers like Firefox and Brave, as well as being deliberately acknowledged by publishers such as the New
York Times and the Washington Post. Most importantly, it was declared by California’s Attorney General as a valid and
committing “Do Not Sell” signal under the California Consumer Protection Act (CCPA) and the California Privacy Rights
Act (CPRA). In practice, it means that, at least in California, the GPC signal serves as a tangible means for users to
protect their privacy and enforce their rights.

In our opinion, GPC is a very cost-efficient utility to achieve more user privacy. While very simple to implement, GPC
empowers users to attest their privacy choice to the outer world, and even tangibly prevents tracking of personal data
on certain websites, and/or by certain companies and in certain prominent jurisdictions.

There are a few initiatives aimed at flagging users’ choices to be accommodated in the ad selection process and at
ensuring compliance with all new privacy regulations. It is worth
mentioning [IAB Tech Lab’s Global Privacy Platform](https://github.com/InteractiveAdvertisingBureau/Global-Privacy-Platform),
which aims to standardize signaling users’ privacy preferences for a range of geographies; this may make sense to adopt
in later stages of the EXPAT approach.

### Spam/Fraud Reduction

#### Email Aliases

**Principles/Goals alignment**:

* Purpose-built for Spam/Fraud Reduction.
* Supports Cooperation.
* Fosters positive Perception.
* Strongly aligns with the first five Principles described above and partially with Fair Value Exchange.

Use of the same email address across multiple websites provides the foundation for cross-site tracking and targeting
when consistent third-party cookies are no longer available. The majority of identity solutions for online advertising
use hashed email addresses as the main cross-site match key. By providing users with a unique address for each website
that requires registration, these email addresses serve a similar purpose as first-party cookies, providing context
regarding one specific domain, rather than behaving like third-party cookies.

An additional benefit for the users is in relation to security and annoyance control: users are able to protect their
primary email address from phishing attacks and are able to filter spam messages more easily.

![Rerouting email](images/rerouting_email.png "image_tooltip")

<sub><strong>Diagram 2: Rerouting email communication through controllable aliases to prevent them from being utilized
as a persistent personal identifier</strong></sub>

The alias@relay.provider convention, proposed here and also offered by[ Mozilla](https://relay.firefox.com/faq)
and[ Apple](https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_js/communicating_using_the_private_email_relay_service),
should be interpreted as a clear statement that the registrant does not want to be tracked across apps and sites, and
registration systems should treat it as such. With the adoption of such services, there is an opportunity to form a
standard that would apply to users and websites, ensuring a consistent functionality that respects this stated user
choice, as well as the consistent utility of privacy-preserving first-party data and consented connection between
publishers and users.

#### Anti-Fingerprinting

**Principles/Goals alignment**:

* Purpose-built for Spam/Fraud Reduction.
* Supports Cooperation.
* Fosters positive Perception.
* Strongly aligns with the six Principles described above.

Although
browsers [have improved their ability to prevent fingerprinting](https://www.apple.com/safari/docs/Safari_White_Paper_Nov_2019.pdf),
with the lack of other identifiers, the incentive to perform this practice would increase and hence, there’s a place for
additional efforts. There are a variety of sources to explain fingerprinting-based tracking around the web. In short,
while a client browses the web, it shares several different data points such as user agent, IP address, screen size,
device model, and others. Potentially, combining all these data points in relation to that client is likely to derive a
unique persistent identifier, as any other client that is observed, would have a different combination.

Fingerprinting-based tracking is very difficult to protect against since browsers cannot block fingerprinting data
points in the same way that the browser can block a request, as it will most likely lead to breaking a large number of
websites.

We believe a better defense mechanism against fingerprinting is privacy by randomization, as described
in [PriVaricator](https://dl.acm.org/doi/abs/10.1145/2736277.2741090)
and [FPRandom](https://hal.inria.fr/hal-01527580/document). This strategy, consisting of making small changes to known
JavaScript browser APIs, used by fingerprinting scripts, proved to offer a stronger and more resilient privacy
protection.

### Targeting

With this strong baseline of privacy, a client-side targeting system changes the balance of online targeting, so that
the client itself is responsible for attesting its own characteristics. In other words, online users can now control and
determine how to be presented to the rest of the online world. With that being said, to support the ad-funded web and
ensure that content creators continue to have means to generate revenue, a client-side ad-targeting solution needs to
ensure that the sites visited are given a share of the value generated from the information used to deliver useful
advertising.

#### Local Profile Generation

**Principles/Goals alignment**:

* Purpose-built for Targeting.
* Supports Cooperation.
* Fosters positive Perception.
* Strongly aligns with the six Principles described above.

As of now, in the cookie-based world, profiles are based on observed activity, registration data, and offline matches.
Finding equivalents for a fully client-side profile can be done as described as follows.

##### Observed Activity

Websites that a user visits and interacts with are commonly used to determine interest categories. These categories are
derived locally, by using a model that maps website content to audience taxonomy protocol. Thus, the user’s browsing
activity is not exposed to any party.

Another valuable source of information is page-related events, or in other words, the user’s engagement with the page’s
content. Based on these events, it is possible to determine the attention level of the user and better understand the
strength of the user’s interest in a specific content category, as long as shifts in preferences along time periods are
taken into account.

##### The profile is built and stored locally on the user’s device and should never leave the device in its raw format, or without applying privacy guarantees to protect against user identification. To fulfill the privacy goals stated above, it’s important that no party has access to users’ information, only the user itself having the capabilities to observe and edit it.

##### Profile Decay Rate

Accounting for shifts in preferences along time periods is important both from the perspective of the advertiser (
including its representatives along the media buying chain) and the user. The advertiser wants to reach an audience that
has a specific current interest. The user is also interested in being presented with advertisements that are relevant.
Adapting the profile to the shifts in the user’s interests over time can be achieved by introducing a decay rate. The
decay rate is the percentage used to decrease periodically the interest scores from the local profile. \
\
The decay rate ensures that the profile reflects the present interests of the user and that old interests’ weights
decrease over time.

##### Location Services

**Principles/Goals alignment**:

* Purpose-built for Targeting.
* Generally supports Cooperation.
* Fosters positive Perception.
* Strongly aligns with the six Principles described above.

Since privacy regulations and practices differ between countries, and sometimes within countries (e.g. USA), it would be
most useful if the technology declares 'country' in most cases, and 'country sub-region' where necessary.

As a reminder, this paper suggests above to proxy ads-related online traffic, which affects the way that location-based
targeting is conducted, since by the majority, this targeting is based on the client’s IP address.

There are two different ways to share location insights by the client:

1. **On-device location and its generalization** to a larger geographic area, such as country, district, zip code, or
   city, extracted from the client’s IP address and added to the bid request for geo-location targeting.
2. **IP-based** geolocation, from a trusted server, returns information limited to country, district, zip code, or city.
   This logic is the one that is widely used by nowadays advertising systems and hence requires less adjustment.

Option 1 is easier to support according to this paper, as IP addresses can be mapped, while in the OpenRTB ad request,
the location code can be shared without the IP address. However, as mentioned above, such a method will require adoption
among buy-side ad servers (namely but not only demand-side platforms), and as a result, can be considered viable only at
a later stage.

Option 2 is harder to maintain but requires fewer adjustments by third parties, and is, therefore, more realistic to
perform. When proxying traffic as described above, web traffic is relayed through a server in proximity to the client’s
real location, while also accounting for k-anonymity considerations. Thus, the bid response would be based on the
client’s location, without exposing its real IP address.

##### User Input

The fact that the user's profile is located at the client opens up new opportunities in relation to the user’s
engagement with its online self, and how such identity can be observed by the rest of the world. Offering the user
control mechanisms for its profile (for example: edit, delete, attest an interest in a specific product), on their
device, could increase the relevance and precision of the data, as well as the user’s trust in it. This can be applied
to inferred interests, geographic information, and also to demographic data.

It is important to mention though, that at a later stage, if the approach proposed in this paper becomes scalable, the
user input data should be balanced by fraud detection, to prevent the arbitrary creation of desirable audiences.

Allowing an individual to provide input about how to **not** be addressed by the outer world is important for the need
to evoke the trust of online society in the web economy. It also corresponds with the spirit of privacy legislation such
as GDPR.

#### Local Profile Disclosure

An advertising profile is only of value if it can be used as input into ad decisioning. A system that offers strong
privacy features, as described, integrates with existing bidding and ad-serving systems with minimal changes, ultimately
bypassing the significant change to operations in proposals
like [Protected Audience API](https://github.com/WICG/turtledove/blob/main/FLEDGE.md).

A system that sends a variable subset of active segment memberships based on a reasonable algorithm offers meaningful
information for decisioning on the ad server to offset the impact of losing advertising identity, while also minimizing
the risk that advertising systems could fingerprint based on segment membership.

![Subset of user interest](images/user_interests_subset.png "image_tooltip")

<sub><strong>Diagram 3: A subset of user interest, processed locally on the device, to be shared in a standard RTB ad
request for the purpose of ad targeting, subject to the privacy guarantees detailed below.</strong></sub>

#### Privacy Guarantees

##### Applying Differential Privacy to Observe Taxonomy Distribution (see diagram 4 below)

For the purpose of preventing the re-identification of an individual user, one option is to apply differential privacy
locally on user data, by adding a controlled random noise to it, followed by the enforcement of k-anonymity on the
denoised data in a trusted server environment.

In favor of simplicity, our current approach for applying noise to the local data is based on the method described
in [Google’s RAPPOR paper](https://github.com/google/rappor). We plan to continue our research in this area to find the
most purposeful approach for applying differential privacy and improve the tradeoff between privacy and utility. The
obtained privacy guarantee, described in the RAPPOR paper, is that an observer cannot accurately determine the user's
original data since the reported data is a combination of the user's data and noise. The probabilities _f_, _p_, and
_q_, found below, control the level of privacy and the trade-off between privacy and utility.

Herein is the simplified high-level overview of the logic behind the noise generation and data encoding process.

1. **Bloom Filter Encoding**: The user's data is encoded into a Bloom filter, a fixed-size bit array that represents a
   set of elements. A hash function maps each element to positions in the bit array. The bits at these positions are set
   to 1 when an element is added to the set. The Bloom filter ensures a compact representation with no false negatives,
   though there may be false positives.
2. **Noise Generation**: Two types of noise are introduced: permanent noise used to calculate the _Permanent Randomized
   Response **PRR** (**B<sup>′<sub>i</sub></sup>**_) and instantaneous noise used to calculate _Instantaneous Randomized
   Response **IRR**_ ( **_P(Si=1) )_**. The permanent noise is generated once and remains constant throughout the user's
   interaction with the system. The instantaneous noise is generated for each response. In both cases, noise is
   generated based on probabilities _f_, _p_, and _q_.
3. **Permanent Randomized Response**: For each category, we pass the original value _B<sub>i </sub>_ as it is with a
   probability of _( 1-f )_. If due to the probability we didn’t pass the original value, we set the value of
   _B<sup>′<sub>i </sub></sup>_ with a 50/50 chance between 0 and 1, in other words with a probability of_ ½ f._  We
   never send this response over the network. \

![Permanent Randomized Response](images/randomized_response.png "image_tooltip")

4. **Response Generation (_Instantaneous Randomized Response_)**: Instead of directly reporting _B<sup>′</sup>_ on every
   request, the client reports a randomized version of _B<sup>′</sup>_. This modification significantly increases the
   difficulty of tracking a client based on _B<sup>′</sup>_, which could otherwise be viewed as a unique identifier in
   longitudinal reporting scenarios. For each category:
    1. If the intermediate result _B<sup>′<sub>i </sub></sup>_ is 1 we set _S<sub>i</sub>=1_ with a probability of _q_.
    2. If the intermediate result _B<sup>′<sub>i </sub></sup>_ is 0 we set _S<sub>i</sub>=1_ with a probability of _p_.

![Response generation](images/response_generation.png "image_tooltip")

    This process ensures that the final response is a mix of the user's noisy data and a second layer of noise, determined by the probabilities _p_ and q.

Trusted Server Denoising and K-anonymity

After collecting the noisy data from users, the data is sent to a trusted server for denoising. The server reconstructs
the true distribution of the data by removing the noise added during the _RAPPOR_ process. Once the correct distribution
is obtained, the server applies k-anonymity to ensure each cohort passes the k-value threshold before publishing the
data in the safe-to-share list. K-anonymity is a privacy model that aims to protect the identity of individuals in a
dataset by ensuring that each record cannot be distinguished from at least k-1 other records.

##### Safe-to-share interests

To meet this paper’s privacy goals, the user interests derived from the local profile should be shared only once they
meet the criteria for k-anonymity. A client implementation shall be informed with information that, based on certain
mathematical logic, ensures that the k-anonymity threshold is indeed met. The approach proposed here is having the
client download a “safe-to-share” list of input on a regular cadence (e.g. daily, every 6 hours, etc.), with the list
containing interests that can be shared and satisfy k-anonymity requirements. The client should share a subset of the
data points from the local profile that are also present in the safe-to-share list, at any given time, in order not to
break the k-anonymity promise. For example, it might tell an advertising system that a user is interested in sports,
cars, and gyms one time, and gaming consoles, cars, and electronics the next, as long as both disclosures are validated
by the “safe-to-share” list that they pass the k-anonymity criteria.

###### Safe-to-share list calculation

![Safe-to-share list calculation](images/safe_to_share_list.jpg "image_tooltip")

<sub><strong>Diagram 4:</strong></sub>

1. <sub><strong>The local user profile is passed to a differential privacy engine.  </strong></sub>
2. <sub><strong>After differential privacy is applied, locally inside the client, a noised version of the local profile
   is generated.</strong></sub>
3. <sub><strong>The noised profile is sent from the client to the k-anonymity server.</strong></sub>
4. <sub><strong>After k-anonymity is computed, a list of interests that can be safely shared without breaking
   k-anonymity is generated.</strong></sub>
5. <sub><strong>The list of interests that can be safely shared is passed back to the client to be used for
   cross-checking during the data-sharing process.</strong></sub>

### Measurement

It is needless to highlight the importance of measurement and attribution for the utility and viability of any ad
system. Much effort has been put in by different parties, especially as part of W3C’s Private Advertising Technology
Community Group (PAT CG). Approaches such as
Google’s [Attribution Reporting API (ARA)](https://github.com/WICG/attribution-reporting-api), Mozilla, and
Meta’s [Interoperable Private Attribution (IPA)](https://github.com/private-attribution/ipa) seem to provide solid paths
to fulfill this paper’s objective in this aspect, and would probably lead to a browser API. Therefore, solving
measurement and attribution in a privacy-preserving manner is not a goal of EXPAT at this point in time. We do, however,
support convergence on one solution and a consistent API if at all possible; consideration should also be given to
mobile and CTV use cases, for future development.

## Future Development

There are a few additional marketing use cases that can be further explored and might be added in future iterations of
EXPAT.

1. **Encryption**

   There are clear benefits in encrypting the information shared via the method which is described in this paper. From a
   privacy perspective, it will limit the number of parties that may access users’ data. Thus, speculatively, if only
   the platform that has won the bid would be able to decrypt the data, the privacy level of the user increases. There
   are other product designs that can apply to the logic of who may access the data, and we intend to approach this use
   case in the future. However, as the OpenRTB protocol doesn’t support encryption for the time being, we would like to
   simplify the examination of the concept expressed in this paper. Hence, with some unease, we will leave this aspect
   for later on.

2. **Remarketing and retargeting**

   Our focus and interest are in client-based determination of remarketing topics. While so far proposals have focused
   on the browser’s role in enabling the local setup of remarketing related interest (
   namely [Protected Audience API](https://github.com/WICG/turtledove/blob/main/FLEDGE.md)
   and [Product-level TURTLEDOVE](https://github.com/WICG/turtledove/blob/main/PRODUCT_LEVEL.md)), our efforts will
   likely focus on a local logic for the client to self-attest interests. That is for two main reasons:

    1. Theoretically, with the right model, the client itself is most capable to determine and attest interests. As
       other proposals already focus on other techniques, we see it necessary to explore this approach.
    2. We could also enable users to actively engage to provide their own input regarding the products they would look
       to consume, and not less importantly, the products they are no longer interested in.

3. [**Seller-Defined Audiences (SDA)**](https://iabtechlab.com/wp-content/uploads/2022/02/IAB-Tech-Lab-Taxonomy-and-Data-Transparency-Standards-to-Support-Seller-defined-Audience-and-Context-Signaling.pdf)

   With a consistent content and interest classification taxonomy and a framework for augmenting ad calls, there is a
   potential to assist publishers with the adoption of SDA, providing relevant interests as an SDA classifier. An
   open-source product, available for all, can raise transparency and trust in the SDA framework. Encryption as
   mentioned in item 1 of this “Future Development” section can solve an additional publisher’s use case, as many
   publishers would rather determine by themselves who exactly can be granted access to audience segments generated in
   relation to their content.

4. **User Panel**

   With the decrease of third-party signals, and more emphasis on first-party ones, the dimension of user interests
   generated outside of that specific website will decrease. In other words, websites can testify for the activity that
   occurs within their domain but can’t provide the context of an activity that occurs externally of that domain. While
   more investment is needed to provide the same privacy guarantees, theoretically, the web economy can benefit from a
   service that can cohort the type of users visiting a certain URL, beyond the direct context, the same as done in
   linear TV.

5. **Privacy Enhancement and the value exchange**

   EXPAT’s aspiration is to create a new equilibrium in which the more privacy that users receive, the more insightful
   data that can durably be shared for the benefit of the web economy. This is a _work-in-progress_: if this paper would
   transform into a scaled product implementation, and would gain sustainable commercial success, then more resources
   can be invested in developing and scaling private solutions for marketing insights. Why is that so? Because stronger
   privacy guarantees regarding an individual client will support more leeway to share data that (1) serves marketing
   use cases but (2) does not enable the re-identification of that individual client.

   **By tying together correlated incentives for user privacy and a sustainable web economy, we aim to achieve long-term
   progress in both fields**, which will be expressed in future iterations of EXPAT.

## Integration Proposal

The approach described in EXPAT can potentially be tested with live implementation, based on one or some of eyeo’s
consumer products and its integrations with publishers.

As such live testing is feasible, herein is how a live integration with third parties is anticipated to work.

Suggested integration options:

1. Utilizing the [Real Time Data module in Prebid.js](https://docs.prebid.org/dev-docs/add-rtd-submodule.html) to enrich
   bid requests with the data from the EXPAT API (see below). This could be a very simple and efficient way of
   integration for publishers and platforms that are using Prebid.js, given the commonness of Prebid among them.
   Furthermore, a similar approach was conducted to make Topics API’s topics available for targeting too.
2. Leveraging <span style="text-decoration:underline;">current integration infrastructure of SSP</span>s

   The SSP code from the webpage could fetch the targeting data from the EXPAT API directly and pass it to the server.
   Such an approach will require coordination with the SSP directly. Its biggest advantage is that executing it is
   feasible already today.

### The EXPAT API

The JavaScript API described in this section should be available on the webpage if a browser extension has EXPAT
implemented and has established communication with a bidder that is registered with the extension provider and approved
by each publisher on the webpage. The purpose of the API is to offer a clearly defined method for accessing targeting
data from EXPAT.

The taxonomy used in this API is the [IAB Audience Taxonomy](https://iabtechlab.com/standards/audience-taxonomy/), which
also was chosen with interoperability in mind so that the integration with the EXPAT API could be more easily adopted.

```javascript
const interests = await window.expat.getInterests();
/**
 {
  interests: [{id: String, taxonomyVersion: String}],
  metadata: {
	country: "DE"
  },
  version: "1.2",
}
 or {interests: [], metadata: {}, version: "1.2"}
 */

// passing interests using an HTTP header
const r = await fetch('url', { expat: true });
// X-Interests: {interests: [], metadata: {}, version: "1.2"}
```

#### Security considerations

* The API should be available only in secure contexts, which means it should be accessible only on websites and frames
  served over HTTPS. This ensures data confidentiality and protects against various attacks.
* Restrict the API availability to vetted pre-registered parties. Limit the availability of
  the `window.expat.getInterests()` function to specific frames from the vetted parties. Allow the addition of
  the `X-Interests` header only on requests made to vetted parties' URLs.
* To ensure a higher level of privacy, once the API is used to obtain interests, subsequent calls from the same website
  will return the same result until the safe-to-share list is refreshed and if the old interests are not safe to share
  anymore.
* Registered parties’ calls are proxied as described above at “Proxying Traffic” in order to limit the possibility of
  re-identifying an individual user.

## Anticipated Next Steps

The motivation behind EXPAT is to propose an additional, extension/client-oriented approach for scalable, sustainable,
privacy-preserving, and user-centric advertising. We aspire to find the balance between (1) user privacy, transparency,
and control, to an extent that is desirable by users and (2) addressability for a thriving ads-supported web economy
that powers the spread of free content.

Assessing the viability of such a vision requires a comprehensive examination of the ideas expressed in this paper,
collection of a variety of feedback, and testing of potential implementations. In the following months, we will not only
seek to collect such feedback but will also actively look for opportunities to experiment with the effectiveness of this
proposal in a scalable testing environment. Such observation may potentially provide the web economy with one more path
for Private Advertising Technology and the evolution of related concepts.

## **APPENDIX**: Proposal Alignment to Principles/Goals

<style>
    table.appendix {
        text-align: center;
    }
   .appendix .red { background: red; }
   .appendix .yellow { background: #f5d887; }
   .appendix .green { background: green; }
</style>

<table class="appendix">
   <tr>
      <td colspan="5" ><span style="text-decoration:underline;">Purpose</span></td>
      <td colspan="6" ><span style="text-decoration:underline;">Principles</span></td>
      <td colspan="4" ><span style="text-decoration:underline;">Goals</span></td>
   </tr>
   <tr>
      <td>Targeting</td>
      <td>Measurement</td>
      <td>Privacy Protection</td>
      <td>Spam / Fraud Reduction</td>
      <td>Compliance</td>
      <td>user Control</td>
      <td>Interop</td>
      <td>Minimal Reinvention</td>
      <td>Zero-trust</td>
      <td>Solution Evolution</td>
      <td>Fair Value Exchange</td>
      <td>Purpose-<p>built</td>
      <td>Cooperation</td>
      <td>Perception</td>
      <td>Principle Alignment</td>
   </tr>
   <tr>
      <td colspan="15">Proxying Traffic</td>
   </tr>
   <tr>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
      <td>X</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="green">green</td>
   </tr>
   <tr>
      <td colspan="15">Cookie/Storage Partitioning</td>
   </tr>
   <tr>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
      <td></td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="red">red</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="red">red</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
   </tr>
   <tr>
      <td colspan="15">Privacy Signals</td>
   </tr>
   <tr>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
      <td>X</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="yellow">yellow</td>
      <td class="yellow">yellow</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
   </tr>
   <tr>
      <td colspan="15">Email Aliases</td>
   </tr>
   <tr>
      <td></td>
      <td></td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
   </tr>
   <tr>
      <td colspan="15">
         Anti-Fingerprinting</td>
   </tr>
   <tr>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
      <td></td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
   </tr>
   <tr>
      <td colspan="15">
         Local Profile Generation</td>
   </tr>
   <tr>
      <td>X</td>
      <td></td>
      <td>X</td>
      <td></td>
      <td>X</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
   </tr>
   <tr>
      <td colspan="15">
         Location Services</td>
   </tr>
   <tr>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
      <td>X</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="green">green</td>
      <td class="yellow">yellow</td>
      <td class="green">green</td>
      <td class="green">green</td>
   </tr>
</table>
